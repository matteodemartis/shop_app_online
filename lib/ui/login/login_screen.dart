import 'package:flutter/material.dart';
import './login_button.dart';

class LoginScreen extends StatelessWidget {
  final String _title = 'Log into Shop App';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_title),
      ),
      body: Center(
        child: LoginButton(),
      ),
    );
  }
}
